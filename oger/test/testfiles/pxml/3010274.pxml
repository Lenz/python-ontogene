<?xml version='1.0' encoding='UTF-8'?>
<MedlineCitation Owner="NLM" Status="MEDLINE">
<PMID Version="1">3010274</PMID>
<DateCreated>
<Year>1986</Year>
<Month>06</Month>
<Day>17</Day>
</DateCreated>
<DateCompleted>
<Year>1986</Year>
<Month>06</Month>
<Day>17</Day>
</DateCompleted>
<DateRevised>
<Year>2013</Year>
<Month>11</Month>
<Day>21</Day>
</DateRevised>
<Article PubModel="Print">
<Journal>
<ISSN IssnType="Print">0143-4004</ISSN>
<JournalIssue CitedMedium="Print">
<Volume>7</Volume>
<Issue>1</Issue>
<PubDate>
<MedlineDate>1986 Jan-Feb</MedlineDate>
</PubDate>
</JournalIssue>
<Title>Placenta</Title>
<ISOAbbreviation>Placenta</ISOAbbreviation>
</Journal>
<ArticleTitle>Histochemical localization of phosphatases in the pig placenta: II. Potassium-dependent and potassium-independent p-nitrophenyl phosphatases at high pH; relation to sodium-potassium-dependent adenosine triphosphatase.</ArticleTitle>
<Pagination>
<MedlinePgn>27-35</MedlinePgn>
</Pagination>
<Abstract>
<AbstractText>Histochemical localization by Mg2+ capture methods of K+-dependent, ouabain-sensitive phosphatase activity in the pig placenta shows that strong Na+,K+-dependent adenosine triphosphatase (Na+,K+-ATPase) activity is restricted to the basal zone of the columnar epithelium covering the areolar chorionic villi. It is proposed that active Na+ absorption at this epithelium may be the source of the ouabain-sensitive, fetal-side-positive potential difference which can be measured across the placental membrane in vitro. The one-step procedure for Na+,K+-ATPase localization is unsatisfactory in this organ as any specific ATPase reaction is swamped by activity probably attributable to uteroferrin and other non-specific phosphatases.</AbstractText>
</Abstract>
<AuthorList CompleteYN="Y">
<Author ValidYN="Y">
<LastName>Firth</LastName>
<ForeName>J A</ForeName>
<Initials>JA</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Sibley</LastName>
<ForeName>C P</ForeName>
<Initials>CP</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Ward</LastName>
<ForeName>B S</ForeName>
<Initials>BS</Initials>
</Author>
</AuthorList>
<Language>eng</Language>
<PublicationTypeList>
<PublicationType UI="D016428">Journal Article</PublicationType>
<PublicationType UI="D013485">Research Support, Non-U.S. Gov't</PublicationType>
</PublicationTypeList>
</Article>
<MedlineJournalInfo>
<Country>ENGLAND</Country>
<MedlineTA>Placenta</MedlineTA>
<NlmUniqueID>8006349</NlmUniqueID>
<ISSNLinking>0143-4004</ISSNLinking>
</MedlineJournalInfo>
<ChemicalList>
<Chemical>
<RegistryNumber>3G0H8C9362</RegistryNumber>
<NameOfSubstance UI="D003035">Cobalt</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.1.3.-</RegistryNumber>
<NameOfSubstance UI="D010744">Phosphoric Monoester Hydrolases</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.1.3.41</RegistryNumber>
<NameOfSubstance UI="D009597">4-Nitrophenylphosphatase</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.6.3.9</RegistryNumber>
<NameOfSubstance UI="D000254">Sodium-Potassium-Exchanging ATPase</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>I38ZP9992A</RegistryNumber>
<NameOfSubstance UI="D008274">Magnesium</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>YZS2RPE8LE</RegistryNumber>
<NameOfSubstance UI="D013324">Strontium</NameOfSubstance>
</Chemical>
</ChemicalList>
<CitationSubset>IM</CitationSubset>
<MeshHeadingList>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D009597">4-Nitrophenylphosphatase</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000032">analysis</QualifierName>
<QualifierName MajorTopicYN="N" UI="Q000378">metabolism</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000818">Animals</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D003035">Cobalt</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D005260">Female</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="Y" UI="D006651">Histocytochemistry</DescriptorName>
<QualifierName MajorTopicYN="N" UI="Q000379">methods</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D006863">Hydrogen-Ion Concentration</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D008274">Magnesium</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D010744">Phosphoric Monoester Hydrolases</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000032">analysis</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D010920">Placenta</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000201">enzymology</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D011247">Pregnancy</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000254">Sodium-Potassium-Exchanging ATPase</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000032">analysis</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D013324">Strontium</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D013552">Swine</DescriptorName>
</MeshHeading>
</MeshHeadingList>
</MedlineCitation>
