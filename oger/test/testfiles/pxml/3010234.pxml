<?xml version='1.0' encoding='UTF-8'?>
<MedlineCitation Owner="NLM" Status="MEDLINE">
<PMID Version="1">3010234</PMID>
<DateCreated>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCreated>
<DateCompleted>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCompleted>
<DateRevised>
<Year>2013</Year>
<Month>10</Month>
<Day>01</Day>
</DateRevised>
<Article PubModel="Print">
<Journal>
<ISSN IssnType="Print">0305-1048</ISSN>
<JournalIssue CitedMedium="Print">
<Volume>14</Volume>
<Issue>8</Issue>
<PubDate>
<Year>1986</Year>
<Month>Apr</Month>
<Day>25</Day>
</PubDate>
</JournalIssue>
<Title>Nucleic acids research</Title>
<ISOAbbreviation>Nucleic Acids Res.</ISOAbbreviation>
</Journal>
<ArticleTitle>The chicken fast skeletal troponin I gene: exon organization and sequence.</ArticleTitle>
<Pagination>
<MedlinePgn>3377-90</MedlinePgn>
</Pagination>
<Abstract>
<AbstractText>The gene encoding the fast skeletal isoform of the chick troponin I (sTnI) protein has been sequenced and its organization into exons and introns established. The gene is approximately 4.5 kb in length and composed of 8 exons, the first of which contains solely 5' untranslated sequence. In addition to its major mRNA product, there is evidence that the sTnI gene encodes a second mRNA, present at low abundance levels in embryonic skeletal muscle. Sl nuclease protection and primer extension experiments indicate that the low abundance mRNA is initiated approximately 47 nucleotides upstream of the major transcriptional initiation site. Both mRNAs appear to encode identical sTnI polypeptides. A comparison of nucleotide sequence in the 5' flanking region of several muscle-specific genes, including the sTnI gene, reveals a heptanucleotide consensus sequence, 5'-CATTCCT-3', which is conserved in the 5' flanking regions of many vertebrate contractile protein genes.</AbstractText>
</Abstract>
<AuthorList CompleteYN="Y">
<Author ValidYN="Y">
<LastName>Nikovits</LastName>
<ForeName>W</ForeName>
<Initials>W</Initials>
<Suffix>Jr</Suffix>
</Author>
<Author ValidYN="Y">
<LastName>Kuncio</LastName>
<ForeName>G</ForeName>
<Initials>G</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Ordahl</LastName>
<ForeName>C P</ForeName>
<Initials>CP</Initials>
</Author>
</AuthorList>
<Language>eng</Language>
<DataBankList CompleteYN="Y">
<DataBank>
<DataBankName>GENBANK</DataBankName>
<AccessionNumberList>
<AccessionNumber>X03832</AccessionNumber>
</AccessionNumberList>
</DataBank>
</DataBankList>
<GrantList CompleteYN="Y">
<Grant>
<GrantID>GM-32018</GrantID>
<Acronym>GM</Acronym>
<Agency>NIGMS NIH HHS</Agency>
<Country>United States</Country>
</Grant>
<Grant>
<GrantID>HL 35561</GrantID>
<Acronym>HL</Acronym>
<Agency>NHLBI NIH HHS</Agency>
<Country>United States</Country>
</Grant>
</GrantList>
<PublicationTypeList>
<PublicationType UI="D016428">Journal Article</PublicationType>
<PublicationType UI="D013487">Research Support, U.S. Gov't, P.H.S.</PublicationType>
</PublicationTypeList>
</Article>
<MedlineJournalInfo>
<Country>ENGLAND</Country>
<MedlineTA>Nucleic Acids Res</MedlineTA>
<NlmUniqueID>0411011</NlmUniqueID>
<ISSNLinking>0305-1048</ISSNLinking>
</MedlineJournalInfo>
<ChemicalList>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D014336">Troponin</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D019210">Troponin I</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>9007-49-2</RegistryNumber>
<NameOfSubstance UI="D004247">DNA</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.1.21.-</RegistryNumber>
<NameOfSubstance UI="D004262">DNA Restriction Enzymes</NameOfSubstance>
</Chemical>
</ChemicalList>
<CitationSubset>IM</CitationSubset>
<CommentsCorrectionsList>
<CommentsCorrections RefType="Cites">
<RefSource>Arch Biochem Biophys. 1974 Jun;162(2):436-41</RefSource>
<PMID Version="1">4407361</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1985 Aug 29-Sep 4;316(6031):774-8</RefSource>
<PMID Version="1">4041012</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochem J. 1976 Feb 1;153(2):375-87</RefSource>
<PMID Version="1">179535</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1977 Dec;74(12):5463-7</RefSource>
<PMID Version="1">271968</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Eur J Biochem. 1978 Jan 16;82(2):493-501</RefSource>
<PMID Version="1">624283</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1978 Jan 5;271(5640):31-5</RefSource>
<PMID Version="1">146828</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1978 Apr;13(4):599-611</RefSource>
<PMID Version="1">657269</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1979 Aug;17(4):879-87</RefSource>
<PMID Version="1">158428</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Eur J Biochem. 1980 Jun;107(2):303-14</RefSource>
<PMID Version="1">6772444</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Can J Biochem. 1980 Aug;58(8):649-54</RefSource>
<PMID Version="1">7459691</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Biol Chem. 1981 Mar 25;256(6):2798-802</RefSource>
<PMID Version="1">6451620</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Annu Rev Biochem. 1981;50:349-83</RefSource>
<PMID Version="1">6791577</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1981 Aug;25(2):385-98</RefSource>
<PMID Version="1">6269744</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Cell Biol. 1981 Nov;91(2 Pt 1):497-504</RefSource>
<PMID Version="1">6171575</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Jan 22;10(2):459-72</RefSource>
<PMID Version="1">7063411</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1982 Mar;79(5):1553-7</RefSource>
<PMID Version="1">6951196</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1982 Aug 26;298(5877):857-9</RefSource>
<PMID Version="1">6287276</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Jul 10;10(13):3861-76</RefSource>
<PMID Version="1">6287424</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1983 Feb 10;301(5900):468-70</RefSource>
<PMID Version="1">6823326</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1983 May 26;303(5915):348-9</RefSource>
<PMID Version="1">6855887</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Science. 1983 Sep 2;221(4614):921-7</RefSource>
<PMID Version="1">6348946</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Methods Enzymol. 1983;101:20-78</RefSource>
<PMID Version="1">6310323</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochemistry. 1983 Aug 16;22(17):4145-52</RefSource>
<PMID Version="1">6615823</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Biol Chem. 1983 Nov 25;258(22):13867-74</RefSource>
<PMID Version="1">6196357</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1983 Dec;35(2 Pt 1):381-92</RefSource>
<PMID Version="1">6317184</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1984 Mar 22-28;308(5957):333-8</RefSource>
<PMID Version="1">6709041</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Dec 10;11(23):8287-301</RefSource>
<PMID Version="1">6324080</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1984 May;81(9):2626-30</RefSource>
<PMID Version="1">6585819</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1983 May;33(1):297-304</RefSource>
<PMID Version="1">6380757</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1984 Nov;39(1):129-40</RefSource>
<PMID Version="1">6091905</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Biol Chem. 1984 Nov 10;259(21):13595-604</RefSource>
<PMID Version="1">6092382</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Science. 1984 Nov 23;226(4677):979-82</RefSource>
<PMID Version="1">6095446</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Mol Cell Biol. 1985 May;5(5):1151-62</RefSource>
<PMID Version="1">4000121</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1985 Feb 25;13(4):1223-37</RefSource>
<PMID Version="1">3855241</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1985 May;82(10):3106-9</RefSource>
<PMID Version="1">3858807</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1985 Dec;82(23):8080-4</RefSource>
<PMID Version="1">3865218</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1985 May 24;13(10):3723-37</RefSource>
<PMID Version="1">4011440</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Cell Biol. 1985 Aug;101(2):618-29</RefSource>
<PMID Version="1">3894379</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Biol Chem. 1985 Sep 15;260(20):11140-8</RefSource>
<PMID Version="1">2993302</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochemistry. 1974 Jun 18;13(13):2697-703</RefSource>
<PMID Version="1">4847540</PMID>
</CommentsCorrections>
</CommentsCorrectionsList>
<MeshHeadingList>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000595">Amino Acid Sequence</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000818">Animals</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D001483">Base Sequence</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D002645">Chickens</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="Y" UI="D003001">Cloning, Molecular</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004247">DNA</DescriptorName>
<QualifierName MajorTopicYN="N" UI="Q000302">isolation &amp; purification</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004262">DNA Restriction Enzymes</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="Y" UI="D005796">Genes</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D009132">Muscles</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000378">metabolism</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D010957">Plasmids</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D014336">Troponin</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D019210">Troponin I</DescriptorName>
</MeshHeading>
</MeshHeadingList>
<OtherID Source="NLM">PMC339779</OtherID>
</MedlineCitation>
