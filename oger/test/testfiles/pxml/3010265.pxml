<?xml version='1.0' encoding='UTF-8'?>
<MedlineCitation Owner="NLM" Status="MEDLINE">
<PMID Version="1">3010265</PMID>
<DateCreated>
<Year>1986</Year>
<Month>06</Month>
<Day>24</Day>
</DateCreated>
<DateCompleted>
<Year>1986</Year>
<Month>06</Month>
<Day>24</Day>
</DateCompleted>
<DateRevised>
<Year>2007</Year>
<Month>11</Month>
<Day>14</Day>
</DateRevised>
<Article PubModel="Print">
<Journal>
<ISSN IssnType="Print">0032-5791</ISSN>
<JournalIssue CitedMedium="Print">
<Volume>65</Volume>
<Issue>3</Issue>
<PubDate>
<Year>1986</Year>
<Month>Mar</Month>
</PubDate>
</JournalIssue>
<Title>Poultry science</Title>
<ISOAbbreviation>Poult. Sci.</ISOAbbreviation>
</Journal>
<ArticleTitle>Causes of mortality in chickens that regressed a Rous sarcoma virus-induced tumor.</ArticleTitle>
<Pagination>
<MedlinePgn>436-40</MedlinePgn>
</Pagination>
<Abstract>
<AbstractText>Rous sarcomas were induced in 6-week-old chickens of several genetically different stocks: inbred lines C, 6(1), 6(3), and 7(2); crosses of inbred lines (6(3) X 7(2)) F4 and (6(1) X 15(1)) F2 X 6(1); and reciprocal crosses (15(1) X 100) F1 X 15(1) and (15(1) X 100) F1 X 100. The resulting tumors were scored for size six times during a 10-week period. Females that had completely regressed their sarcomas were placed in individual laying cages and examined weekly for reappearance of a tumor. After death, the probable cause was determined by necropsy. The major causes of death in the pooled sample of 49 females were fatty liver hemorrhagic syndrome (24.6%), reproductive disorder (14.2%), Marek's disease (12.2%), and lymphoid leukosis (6.1%). Elapsed time between tumor regression and death from any cause ranged from 21 days to 1930 days (5.3 years). One tumor recurred, this in a bird which eventually died with a massive sarcoma in the left wingweb and Rous metastasis in liver tissue. These data provide evidence of specific resistance to neoplastic disease.</AbstractText>
</Abstract>
<AuthorList CompleteYN="Y">
<Author ValidYN="Y">
<LastName>Collins</LastName>
<ForeName>W M</ForeName>
<Initials>WM</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Ward</LastName>
<ForeName>P H</ForeName>
<Initials>PH</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Dunlop</LastName>
<ForeName>W R</ForeName>
<Initials>WR</Initials>
</Author>
</AuthorList>
<Language>eng</Language>
<GrantList CompleteYN="Y">
<Grant>
<GrantID>CA 17680</GrantID>
<Acronym>CA</Acronym>
<Agency>NCI NIH HHS</Agency>
<Country>United States</Country>
</Grant>
</GrantList>
<PublicationTypeList>
<PublicationType UI="D016428">Journal Article</PublicationType>
<PublicationType UI="D013485">Research Support, Non-U.S. Gov't</PublicationType>
<PublicationType UI="D013487">Research Support, U.S. Gov't, P.H.S.</PublicationType>
</PublicationTypeList>
</Article>
<MedlineJournalInfo>
<Country>UNITED STATES</Country>
<MedlineTA>Poult Sci</MedlineTA>
<NlmUniqueID>0401150</NlmUniqueID>
<ISSNLinking>0032-5791</ISSNLinking>
</MedlineJournalInfo>
<CitationSubset>IM</CitationSubset>
<MeshHeadingList>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000818">Animals</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D002645">Chickens</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D005260">Female</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D009365">Neoplasm Regression, Spontaneous</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000662">veterinary</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D011201">Poultry Diseases</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000401">mortality</QualifierName>
<QualifierName MajorTopicYN="N" UI="Q000503">physiopathology</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D001357">Sarcoma, Avian</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000401">mortality</QualifierName>
<QualifierName MajorTopicYN="N" UI="Q000503">physiopathology</QualifierName>
</MeshHeading>
</MeshHeadingList>
</MedlineCitation>
