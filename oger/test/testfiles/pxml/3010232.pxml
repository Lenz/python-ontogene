<?xml version='1.0' encoding='UTF-8'?>
<MedlineCitation Owner="NLM" Status="MEDLINE">
<PMID Version="1">3010232</PMID>
<DateCreated>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCreated>
<DateCompleted>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCompleted>
<DateRevised>
<Year>2013</Year>
<Month>10</Month>
<Day>01</Day>
</DateRevised>
<Article PubModel="Print">
<Journal>
<ISSN IssnType="Print">0305-1048</ISSN>
<JournalIssue CitedMedium="Print">
<Volume>14</Volume>
<Issue>8</Issue>
<PubDate>
<Year>1986</Year>
<Month>Apr</Month>
<Day>25</Day>
</PubDate>
</JournalIssue>
<Title>Nucleic acids research</Title>
<ISOAbbreviation>Nucleic Acids Res.</ISOAbbreviation>
</Journal>
<ArticleTitle>Supercoil induced S1 hypersensitive sites in the rat and human ribosomal RNA genes.</ArticleTitle>
<Pagination>
<MedlinePgn>3263-77</MedlinePgn>
</Pagination>
<Abstract>
<AbstractText>Rat and human ribosomal RNA gene fragments in supercoiled plasmids were examined for S1 nuclease hypersensitivity. In the transcribed portion of genes the number and distribution of S1 sites were found to be species specific. No S1 sites were detected in the promoter regions. In the nontranscribed spacer (NTS), downstream of the 3' end of 28S RNA gene, S1 sites appear to be conserved in rat and human rDNAs. A rat NTS fragment (2987 nucleotides long), containing three S1 sites was sequenced and the S1 sites in this region were localized in polypyrimidine . polypurine simple repeat sequences. Other types of simple sequences, two type 2 Alu repeats and an ID sequence were also found in the sequenced region. The possible role of simple sequences and S1 sites in transcription and in recombination events of rDNA is discussed.</AbstractText>
</Abstract>
<AuthorList CompleteYN="Y">
<Author ValidYN="Y">
<LastName>Financsek</LastName>
<ForeName>I</ForeName>
<Initials>I</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Tora</LastName>
<ForeName>L</ForeName>
<Initials>L</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Kelemen</LastName>
<ForeName>G</ForeName>
<Initials>G</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Hidvégi</LastName>
<ForeName>E J</ForeName>
<Initials>EJ</Initials>
</Author>
</AuthorList>
<Language>eng</Language>
<DataBankList CompleteYN="Y">
<DataBank>
<DataBankName>GENBANK</DataBankName>
<AccessionNumberList>
<AccessionNumber>X03838</AccessionNumber>
</AccessionNumberList>
</DataBank>
</DataBankList>
<PublicationTypeList>
<PublicationType UI="D003160">Comparative Study</PublicationType>
<PublicationType UI="D016428">Journal Article</PublicationType>
<PublicationType UI="D013485">Research Support, Non-U.S. Gov't</PublicationType>
</PublicationTypeList>
</Article>
<MedlineJournalInfo>
<Country>ENGLAND</Country>
<MedlineTA>Nucleic Acids Res</MedlineTA>
<NlmUniqueID>0411011</NlmUniqueID>
<ISSNLinking>0305-1048</ISSNLinking>
</MedlineJournalInfo>
<ChemicalList>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D004275">DNA, Ribosomal</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D004278">DNA, Superhelical</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D012335">RNA, Ribosomal</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.1.-</RegistryNumber>
<NameOfSubstance UI="D004720">Endonucleases</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>EC 3.1.30.1</RegistryNumber>
<NameOfSubstance UI="D015719">Single-Strand Specific DNA and RNA Endonucleases</NameOfSubstance>
</Chemical>
</ChemicalList>
<CitationSubset>IM</CitationSubset>
<CommentsCorrectionsList>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1977 Feb;74(2):560-4</RefSource>
<PMID Version="1">265521</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1981 Dec;27(3 Pt 2):413-5</RefSource>
<PMID Version="1">6101195</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1978 Jul;14(3):655-71</RefSource>
<PMID Version="1">688387</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1978 Nov;75(11):5367-71</RefSource>
<PMID Version="1">364477</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1978 Nov;15(3):1033-44</RefSource>
<PMID Version="1">728984</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Eur J Biochem. 1979 Jul;98(1):301-7</RefSource>
<PMID Version="1">467445</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1980 Oct;21(3):627-38</RefSource>
<PMID Version="1">7438203</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1980 Nov;77(11):6468-72</RefSource>
<PMID Version="1">6256738</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1981 Feb 5;289(5797):466-70</RefSource>
<PMID Version="1">7464915</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Sheng Li Ke Xue Jin Zhan. 1980 Apr;11(4):289-95</RefSource>
<PMID Version="1">6266008</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1982 Feb 25;295(5851):714-6</RefSource>
<PMID Version="1">6276782</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Jun 11;10(11):3353-70</RefSource>
<PMID Version="1">6285297</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1982 Sep 23;299(5881):312-6</RefSource>
<PMID Version="1">6287292</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Jun 25;10(12):3667-80</RefSource>
<PMID Version="1">6287418</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1982 Jun;29(2):609-22</RefSource>
<PMID Version="1">6288265</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Gene. 1982 May;18(2):115-22</RefSource>
<PMID Version="1">6290316</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochem Biophys Res Commun. 1982 Aug 31;107(4):1571-6</RefSource>
<PMID Version="1">6753847</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Dec 11;10(23):7593-608</RefSource>
<PMID Version="1">6218482</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1983 Apr;32(4):1191-203</RefSource>
<PMID Version="1">6301683</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Aug 25;11(16):5361-80</RefSource>
<PMID Version="1">6310495</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Sep 24;11(18):6437-52</RefSource>
<PMID Version="1">6312424</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1983 Oct 25;170(2):567-73</RefSource>
<PMID Version="1">6313945</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1983 Dec 1-7;306(5942):483-7</RefSource>
<PMID Version="1">6417547</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1984 Feb;36(2):413-22</RefSource>
<PMID Version="1">6319022</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1984 Feb 5;172(4):417-36</RefSource>
<PMID Version="1">6319718</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nature. 1984 Mar 15-21;308(5956):237-41</RefSource>
<PMID Version="1">6199680</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1984 Apr;36(4):933-42</RefSource>
<PMID Version="1">6323028</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Dec 10;11(23):8495-508</RefSource>
<PMID Version="1">6231528</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Biol Chem. 1984 Jun 10;259(11):7268-74</RefSource>
<PMID Version="1">6202693</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1984 May 25;12(10):4127-38</RefSource>
<PMID Version="1">6328411</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1984 Nov 12;12(21):8059-72</RefSource>
<PMID Version="1">6095187</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1984 Nov 26;12(22):8489-507</RefSource>
<PMID Version="1">6504702</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1984 Nov 5;179(3):469-96</RefSource>
<PMID Version="1">6096552</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1985 Jun;41(2):541-51</RefSource>
<PMID Version="1">2985282</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Cell. 1985 May;41(1):21-30</RefSource>
<PMID Version="1">2986843</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1985 Feb 25;13(4):1135-49</RefSource>
<PMID Version="1">2987817</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1985 May 25;183(2):213-23</RefSource>
<PMID Version="1">2989541</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1985 Jun 25;183(4):519-27</RefSource>
<PMID Version="1">2991535</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1985 Jul 20;184(2):195-210</RefSource>
<PMID Version="1">2993630</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1985 Aug 5;184(3):389-98</RefSource>
<PMID Version="1">3862875</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Gene. 1985;36(3):249-62</RefSource>
<PMID Version="1">3000877</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Acta Biochim Biophys Acad Sci Hung. 1984;19(3-4):185-92</RefSource>
<PMID Version="1">6100664</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1977 Jun 15;113(1):237-51</RefSource>
<PMID Version="1">881736</PMID>
</CommentsCorrections>
</CommentsCorrectionsList>
<MeshHeadingList>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000818">Animals</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D001483">Base Sequence</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D003001">Cloning, Molecular</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004275">DNA, Ribosomal</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004278">DNA, Superhelical</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004720">Endonucleases</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000378">metabolism</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="Y" UI="D005796">Genes</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D006801">Humans</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D009693">Nucleic Acid Hybridization</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="Y" UI="D010957">Plasmids</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D014176">Protein Biosynthesis</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D012335">RNA, Ribosomal</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D051381">Rats</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D015719">Single-Strand Specific DNA and RNA Endonucleases</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D013045">Species Specificity</DescriptorName>
</MeshHeading>
</MeshHeadingList>
<OtherID Source="NLM">PMC339761</OtherID>
</MedlineCitation>
