<?xml version='1.0' encoding='UTF-8'?>
<MedlineCitation Owner="NLM" Status="MEDLINE">
<PMID Version="1">3010231</PMID>
<DateCreated>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCreated>
<DateCompleted>
<Year>1986</Year>
<Month>06</Month>
<Day>13</Day>
</DateCompleted>
<DateRevised>
<Year>2013</Year>
<Month>10</Month>
<Day>01</Day>
</DateRevised>
<Article PubModel="Print">
<Journal>
<ISSN IssnType="Print">0305-1048</ISSN>
<JournalIssue CitedMedium="Print">
<Volume>14</Volume>
<Issue>8</Issue>
<PubDate>
<Year>1986</Year>
<Month>Apr</Month>
<Day>25</Day>
</PubDate>
</JournalIssue>
<Title>Nucleic acids research</Title>
<ISOAbbreviation>Nucleic Acids Res.</ISOAbbreviation>
</Journal>
<ArticleTitle>The stiffness of dsRNA: hydrodynamic studies on fluorescence-labelled RNA segments of bovine rotavirus.</ArticleTitle>
<Pagination>
<MedlinePgn>3215-28</MedlinePgn>
</Pagination>
<Abstract>
<AbstractText>The sedimentation coefficients of dsRNA segments of bovine rotavirus were determined in the analytical ultracentrifuge. The eleven segments were separated by preparative gel electrophoresis, and isolated by elution from gel pieces. The RNA was labelled by the intercalating fluorescent dye ethidium bromide at a ratio bound dye per base pair between 0.003 to 0.018. The analytical ultracentrifuge was equipped with a fluorescence recording optics. Sedimentation coefficients could be determined with amounts of RNA as little as 8 ng. All sedimentation coefficients were extrapolated to zero-concentration, zero-dye binding, and zero-impurities from the preparative gel electrophoresis. The hydrodynamic model of flexible cylinders was applied for the interpretation of the sedimentation coefficients. All dsRNA segments of rotavirus (663-3409 base pairs) and the dsRNA5 of cucumber mosaic virus (335 base pairs) fit the model of a "worm-like" or flexible cylinder with a persistence length of 1125 A and a hydrated diameter of 30 A. The results are compared with data from the literature on the persistence lengths of the B- and Z-forms of dsDNA and of viroids.</AbstractText>
</Abstract>
<AuthorList CompleteYN="Y">
<Author ValidYN="Y">
<LastName>Kapahnke</LastName>
<ForeName>R</ForeName>
<Initials>R</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Rappold</LastName>
<ForeName>W</ForeName>
<Initials>W</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Desselberger</LastName>
<ForeName>U</ForeName>
<Initials>U</Initials>
</Author>
<Author ValidYN="Y">
<LastName>Riesner</LastName>
<ForeName>D</ForeName>
<Initials>D</Initials>
</Author>
</AuthorList>
<Language>eng</Language>
<PublicationTypeList>
<PublicationType UI="D016428">Journal Article</PublicationType>
<PublicationType UI="D013485">Research Support, Non-U.S. Gov't</PublicationType>
</PublicationTypeList>
</Article>
<MedlineJournalInfo>
<Country>ENGLAND</Country>
<MedlineTA>Nucleic Acids Res</MedlineTA>
<NlmUniqueID>0411011</NlmUniqueID>
<ISSNLinking>0305-1048</ISSNLinking>
</MedlineJournalInfo>
<ChemicalList>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D012330">RNA, Double-Stranded</NameOfSubstance>
</Chemical>
<Chemical>
<RegistryNumber>0</RegistryNumber>
<NameOfSubstance UI="D012367">RNA, Viral</NameOfSubstance>
</Chemical>
</ChemicalList>
<CitationSubset>IM</CitationSubset>
<CommentsCorrectionsList>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1968 Jul 28;35(2):251-90</RefSource>
<PMID Version="1">4107107</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biosci Rep. 1985 Mar;5(3):251-65</RefSource>
<PMID Version="1">4016225</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1976 Jun 14;104(1):109-44</RefSource>
<PMID Version="1">957429</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Mol Biol. 1976 Jun 14;104(1):145-67</RefSource>
<PMID Version="1">957430</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochemistry. 1977 Apr 5;16(7):1490-8</RefSource>
<PMID Version="1">191072</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochemistry. 1978 Nov 28;17(24):5078-88</RefSource>
<PMID Version="1">569495</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Plasmid. 1979 Jul;2(3):303-22</RefSource>
<PMID Version="1">384415</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biochim Biophys Acta. 1979 Sep 27;564(2):275-88</RefSource>
<PMID Version="1">486481</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Virol. 1979 Nov;32(2):593-605</RefSource>
<PMID Version="1">228080</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Methods Enzymol. 1980;65(1):499-560</RefSource>
<PMID Version="1">6246368</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Q Rev Biophys. 1981 Feb;14(1):81-139</RefSource>
<PMID Version="1">7025081</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1981 Nov;78(11):6808-11</RefSource>
<PMID Version="1">6947255</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biopolymers. 1981 Dec;20(12):2509-31</RefSource>
<PMID Version="1">7326358</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Biopolymers. 1981 Dec;20(12):2671-90</RefSource>
<PMID Version="1">7034800</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1982 Sep 25;10(18):5587-98</RefSource>
<PMID Version="1">7145708</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Med Virol. 1983;11(1):39-52</RefSource>
<PMID Version="1">6300317</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Mar 25;11(6):1919-30</RefSource>
<PMID Version="1">6835843</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Proc Natl Acad Sci U S A. 1983 Jan;80(2):373-7</RefSource>
<PMID Version="1">6300836</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 May 25;11(10):3351-62</RefSource>
<PMID Version="1">6304629</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1983 Jul 25;11(14):4689-701</RefSource>
<PMID Version="1">6308556</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Gen Virol. 1984 Jan;65 ( Pt 1):233-9</RefSource>
<PMID Version="1">6319571</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Virology. 1984 Apr 15;134(1):249-53</RefSource>
<PMID Version="1">6324473</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Virol. 1984 Jul;51(1):97-101</RefSource>
<PMID Version="1">6328048</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Intervirology. 1984;22(1):17-23</RefSource>
<PMID Version="1">6735661</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Nucleic Acids Res. 1984 Aug 10;12(15):6231-46</RefSource>
<PMID Version="1">6473106</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Virology. 1985 May;143(1):55-62</RefSource>
<PMID Version="1">4060583</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Virology. 1984 Oct 15;138(1):178-82</RefSource>
<PMID Version="1">6093360</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Virus Res. 1984 Oct;1(7):533-41</RefSource>
<PMID Version="1">6099939</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>Virus Res. 1985 Mar;2(2):175-82</RefSource>
<PMID Version="1">2986375</PMID>
</CommentsCorrections>
<CommentsCorrections RefType="Cites">
<RefSource>J Virol. 1976 May;18(2):652-8</RefSource>
<PMID Version="1">5615</PMID>
</CommentsCorrections>
</CommentsCorrectionsList>
<MeshHeadingList>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D000818">Animals</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D002417">Cattle</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D002460">Cell Line</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D004591">Electrophoresis, Polyacrylamide Gel</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D008970">Molecular Weight</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D009690">Nucleic Acid Conformation</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D012330">RNA, Double-Stranded</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000302">isolation &amp; purification</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D012367">RNA, Viral</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000302">isolation &amp; purification</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D012401">Rotavirus</DescriptorName>
<QualifierName MajorTopicYN="Y" UI="Q000235">genetics</QualifierName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D013050">Spectrometry, Fluorescence</DescriptorName>
</MeshHeading>
<MeshHeading>
<DescriptorName MajorTopicYN="N" UI="D014461">Ultracentrifugation</DescriptorName>
</MeshHeading>
</MeshHeadingList>
<OtherID Source="NLM">PMC339748</OtherID>
</MedlineCitation>
